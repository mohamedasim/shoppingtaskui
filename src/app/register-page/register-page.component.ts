import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCVM } from '../Models/AllVM';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { UserService } from '../Services/user.service';
@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {
  RegisterForm:FormGroup= this.fb.group({
    UserName:[null,[Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Z]*')]],
    Password:[null,[Validators.required,Validators.minLength(4)]]
  });
  constructor(private fb:FormBuilder,private userService:UserService,
    private route:ActivatedRoute,private router:Router,
    public loading:LoadingService,public notification:NotificationService) { }

  ngOnInit(): void {
  }

  onSubmit(submitData:UserCVM){debugger
    this.loading.isLoading(true);
    this.userService.postUser(submitData).subscribe(res=>{
      this.loading.isLoading(false);
      this.notification.showSuccess("Registered Successfully!")
this.router.navigate(['/login'],{relativeTo:this.route});
    },(error =>{
      this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
    })    
    );
  }
}
