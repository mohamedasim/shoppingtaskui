import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { AddressFormService} from '../Services/address-form.service';
import { Observable } from 'rxjs';
import { AddressCVM, AddressUVM, AddressVM, CustomerVM } from '../Models/AllVM';
import { AddressService } from '../Services/Address.service';
import { CustomerService } from '../Services/customer.service';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { SwalService} from '../Services/swal.service';
@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit {
 Address:AddressVM[] =[];
  Customers:CustomerVM[];
  Customer:CustomerVM;
  isEdit:boolean=false;

  @Input() Addressid:number =null;
  @Output() closeAddressform:EventEmitter<any>=new EventEmitter();
  @Input() AddressIdFromCusDetail:number =null;
  @Input() CusId:number=null;
  @Input() display:string="none";
  @Input() deleteId:number=null;


  AddressForm:FormGroup=this.fb.group({
    RecId:[null],
    CustomerId:[null,[Validators.required]],
    Street:[null,[Validators.required]],
    Country:[null,[Validators.required]],
    City:[null,[Validators.required]],
  });
  constructor(private fb:FormBuilder,public addressFormService:AddressFormService,
    public AddressService:AddressService,
      private customerService:CustomerService,
      public loading:LoadingService,public notification:NotificationService,
      public swalService:SwalService) { }

  ngOnInit(): void {
    console.log(this.Addressid);
    this.getCustomerdata();
    if(this.Addressid||this.AddressIdFromCusDetail){
      this.edit();
    }
    // if(this.deleteId){
    //   this.delete(this.deleteId);
    // }
  }
  openModel(){
    this.display='block'; 
  }
      closeModel(){
        this.display='none';
        this.AddressForm.reset();
        this.isEdit=false;
        this.Addressid=null;
        this.deleteId=null;
        this.closeAddressform.emit();
      }
      getCustomerdata(){
        if(!this.CusId){
          this.loading.isLoading(true);
          let data: Observable<CustomerVM[]>;
          data =this.customerService.getAllCustomers();
          data.subscribe(
            (res:any)=>{
              this.Customers = res;
              this.loading.isLoading(false);
            },
            (error =>{
              this.loading.isLoading(false);
              this.notification.ShowError(error.error.ExceptionMessage,error.message);
            })
          )
        }
        else{
          this.loading.isLoading(true);
    let data: Observable<CustomerVM>;
    data =this.customerService.getCustomerByRecId(this.CusId);
    data.subscribe(
    (res =>{
      this.Customer=res;
      this.loading.isLoading(false);
      this.AddressForm.patchValue({CustomerId:this.CusId});
    }),
    (err =>{
      this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
    })
  );
}
}
      submit(submitData:AddressVM){debugger;
        this.loading.isLoading(true);
    if(submitData.RecId == null||submitData.RecId == undefined){
      const data:AddressCVM = {
        CustomerId:submitData.CustomerId,
        Street:submitData.Street,
        Country:submitData.Country,
        City:submitData.City
      };
       this.AddressService.createAddress(data).subscribe(
         (res =>{
           this.AddressForm.reset();
           this.closeModel();
           this.loading.isLoading(false);
           this.notification.showSuccess("Address Created Successfully.");
           //this.getTabledata();
         }),
         (error=>{
           this.AddressForm.reset();
           this.closeModel();
           this.loading.isLoading(false);
           this.notification.ShowError(error.error.ExceptionMessage,error.message);
         }),
       )
    }
    else{
      this.loading.isLoading(true);
      const data:AddressUVM={
        RecId:submitData.RecId,
        CustomerId:submitData.CustomerId,
        Street:submitData.Street,
        Country:submitData.Country,
        City:submitData.City
      }
      this.AddressService.updateAddress(data).subscribe(
        (res =>{
          this.AddressForm.reset();
          this.closeModel();
          this.loading.isLoading(false);
          this.notification.showSuccess("Address Updated Successfully.");
          //this.getTabledata();
        }),
        (error =>{
          this.AddressForm.reset();
          this.closeModel();
          this.loading.isLoading(false);
          this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      );
    }
      }
      AddId:number=null;
      edit(){debugger

        if(this.Addressid) {
          
          this.AddId=this.Addressid;
        }
        if(this.AddressIdFromCusDetail){
          this.AddId=this.AddressIdFromCusDetail;
        }
        if(!this.AddId)
        return
        this.isEdit=true;
        this.loading.isLoading(true);
        this.AddressService.getAddressByRecId(this.AddId).subscribe(
          (res =>{
            this.AddressForm.patchValue(res);
            this.openModel();
            this.loading.isLoading(false);
          }),
          (err =>{
            this.loading.isLoading(false);
            this.notification.ShowError(err.error.ExceptionMessage,err.message);
          })
        )
      }
      delete(RecId:number){debugger
        // if(!confirm("Are you sure want to delete?"))
        // return;
        this.loading.isLoading(true);
        this.AddressService.deleteAddress(RecId).subscribe(
          (res =>{
            this.loading.isLoading(false);
            this.notification.showSuccess("Address Deleted Successfully");
          }),
          (error =>{
            this.loading.isLoading(false);
            this.notification.ShowError(error.error.ExceptionMessage,error.message);
          })
        );
      }  
}
