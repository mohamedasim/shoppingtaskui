import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  loginName="";
  Loginsts:boolean;
  constructor(public userService:UserService) { }
  ngOnInit(): void {
    this.loginName=this.userService.getlocalloginname();
  }
  logincheck(){
    this.userService.StoreloginStatus(false);
    // this.userService.StoreloginStatus(true);
  }
  loginSts(){
    this.userService.StoreloginStatus(false);
  }
}
