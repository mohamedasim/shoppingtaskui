import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { AddressVM, CustomerVM, ePaymentType, OrderCVM, OrderUVM, OrderVM } from '../Models/AllVM';
import { OrderService } from '../Services/Order.service';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { AddressService } from '../Services/Address.service';
import { CustomerService } from '../Services/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css']
})
export class OrderPageComponent implements OnInit {
  Orders:OrderVM[];
  isEdit:boolean=false;
  Address:AddressVM[];
  Customers:CustomerVM[];
    constructor(private OrderService:OrderService,
      private CustomerService:CustomerService,
      private fb:FormBuilder,private AddressService:AddressService,
      private route:ActivatedRoute,private router:Router,
      public loading:LoadingService,public notification:NotificationService) {
     }
    OrderForm:FormGroup=this.fb.group({
      RecId:[null],
      Description:[null],
      CustomerId:[null,[Validators.required]],
      AddressId:[null,[Validators.required]],
      PaymentType:[null,[Validators.required]],
      OrderDate:[null]
    })
    ngOnInit(): void {
      this.getTabledata();
      this.getCustomerdata();
    }
  
    getTabledata(){
      this.loading.isLoading(true);
      let data: Observable<OrderVM[]>;
      data =this.OrderService.getAllOrders();
      data.subscribe(
        (res)=>{
          this.Orders = res;
          this.loading.isLoading(false);
        },
        (error =>{
    this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }

    getCustomerdata(){
      this.loading.isLoading(true);
      let data: Observable<CustomerVM[]>;
      data =this.CustomerService.getAllCustomers();
      data.subscribe(
        (res)=>{
          this.Customers = res;
          this.loading.isLoading(false);
        },
        (error =>{
          this.loading.isLoading(false);
            this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }
    getAddressdata(){
      this.loading.isLoading(true);
      let data: Observable<AddressVM[]>;
      data =this.AddressService.getAllAddress();
      data.subscribe(
        (res)=>{
         // this.Address = res;
    this.loading.isLoading(false);
        },
        (error =>{
          this.loading.isLoading(false);
            this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }

    getAddressBasedOnCustomer(){ 
      debugger;
      this.loading.isLoading(true);
     let cusid = this.OrderForm.value['CustomerId']
      let data: Observable<AddressVM[]>;
      data =this.AddressService.getAddressBasedOnCustomerRecId(cusid)
      data.subscribe(
      (res =>{
        this.Address=res;
    this.loading.isLoading(false);
      }),
      (err =>{
        this.loading.isLoading(false);
        this.notification.ShowError(err.error.ExceptionMessage,err.message);
      })
    );
    }
    submit(submitData:OrderVM){debugger;
    this.loading.isLoading(true);
  if(submitData.RecId == null||submitData.RecId == undefined){
    const data:OrderCVM = {
      Description:submitData.Description,
      CustomerId:submitData.CustomerId,
      AddressId:submitData.AddressId,
      PaymentType:submitData.PaymentType,
      OrderDate:submitData.OrderDate
    };
     this.OrderService.createOrder(data).subscribe(
       (res =>{
         this.closeModel();
         this.OrderForm.reset();
         this.loading.isLoading(false);
         this.notification.showSuccess("Order Created Successfully.");
         this.getTabledata();
       }),
       (error=>{
         this.closeModel();
         this.OrderForm.reset();
         this.loading.isLoading(false);
           this.notification.ShowError(error.error.ExceptionMessage,error.message);
       }),
     )
  }
  else{
    this.loading.isLoading(true);
    const data:OrderUVM={
      RecId:submitData.RecId,
      Description:submitData.Description,
      CustomerId:submitData.CustomerId,
      AddressId:submitData.AddressId,
      PaymentType:submitData.PaymentType,
      OrderDate:submitData.OrderDate
    }
    this.OrderService.updateOrder(data).subscribe(
      (res =>{
        this.closeModel();
        this.OrderForm.reset();
        this.loading.isLoading(false);
        this.notification.showSuccess("Product Updated Successfully");
        this.getTabledata();

      }),
      (error =>{
        this.closeModel();
        this.OrderForm.reset();
    this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    );
  }
    }
display='none';
    openModel(){
      this.display='block'; 
    }
closeModel(){
  this.display='none'; 
  this.OrderForm.reset();
  this.isEdit=false;
}
edit(RecId:number){debugger
  this.isEdit=true;
  this.loading.isLoading(true);
  this.OrderService.getOrderByRecId(RecId).subscribe(
    (res =>{
      this.OrderForm.patchValue(res);
      this.getAddressBasedOnCustomer();
      this.openModel();
      this.loading.isLoading(false);
    }),
    (err =>{
      this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
    })
  )
}
delete(RecId:number){debugger
      if(!confirm("Are you sure want to delete?"))
      return;
      this.loading.isLoading(true);
      this.OrderService.deleteOrder(RecId).subscribe(
        (res =>{
          this.loading.isLoading(false);
          this.notification.showSuccess("Order Deleted Successfully.");
          this.getTabledata();
        }),
        (error =>{
          this.loading.isLoading(false);
            this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      );
  
    }
    goDetails(OrdId:number){
      this.router.navigate(['/Order-details',OrdId]);
    }
}
