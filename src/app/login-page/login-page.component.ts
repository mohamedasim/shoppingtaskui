import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCVM} from '../Models/AllVM';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { UserService } from '../Services/user.service';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
 log:boolean=true;
  LoginForm:FormGroup= this.fb.group({
    UserName:[null,[Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Z]*')]],
    Password:[null,[Validators.required,Validators.minLength(4)]]
  });
  constructor(private fb:FormBuilder,private userService:UserService,private route:ActivatedRoute,
    private router:Router,public loading:LoadingService,public notification:NotificationService) { }
  ngOnInit(): void {
  }
  onSubmit(submitData:UserCVM){
    this.loading.isLoading(true);
    this.userService.validateUser(submitData).subscribe(res =>{
      this.userService.StoreloginStatus(true);
      this.userService.Storeloginname(submitData.UserName);
      this.loading.isLoading(false);
      this.notification.showSuccess("Logged in Successfully");
      this.router.navigate(['/Customer']);
    },(error =>{
      this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
    }))
  }
}
