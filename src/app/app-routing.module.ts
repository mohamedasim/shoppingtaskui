import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressPageComponent } from './address-page/address-page.component';
import { CustomerDetailsPageComponent } from './customer-details-page/customer-details-page.component';
import { CustomerPageComponent } from './customer-page/customer-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { OrderDetailsPageComponent } from './order-details-page/order-details-page.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { UserService } from './Services/user.service';


const routes: Routes = [
  {path:"",component:CustomerPageComponent,canActivate:[UserService]},
  {path:"Login",component:LoginPageComponent},
  {path:"Register",component:RegisterPageComponent},
  {path:"Product",component:ProductsPageComponent,canActivate:[UserService]},
  {path:"Customer",component:CustomerPageComponent,canActivate:[UserService]},
  {path:"Customer-details/:RecId",component:CustomerDetailsPageComponent,canActivate:[UserService]},
  {path:"Order",component:OrderPageComponent,canActivate:[UserService]},
  {path:"Order-details/:OrdId",component:OrderDetailsPageComponent,canActivate:[UserService]},
  {path:"Address",component:AddressPageComponent,canActivate:[UserService]},
  {path:"**",redirectTo:"Login"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor(){}
 }
