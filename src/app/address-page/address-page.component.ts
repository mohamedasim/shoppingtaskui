import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AddressCVM, AddressUVM, AddressVM, CustomerVM } from '../Models/AllVM';
import { AddressService } from '../Services/Address.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from '../Services/customer.service';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { AddressFormService } from '../Services/address-form.service';
import { SwalService } from '../Services/swal.service';

@Component({
  selector: 'app-address-page',
  templateUrl: './address-page.component.html',
  styleUrls: ['./address-page.component.css']
})
export class AddressPageComponent implements OnInit {
  Address: AddressVM[];
  isEdit: boolean = false;
  Customers: CustomerVM[];
  constructor(public AddressService: AddressService, private fb: FormBuilder,
    private customerService: CustomerService,
    public loading: LoadingService, public notification: NotificationService, 
    public addressFormService: AddressFormService,public swalService:SwalService) { }

  ngOnInit(): void {
    this.getTabledata();
    this.getCustomerdata();
  }

  getTabledata() {
    this.loading.isLoading(true);
    let data: Observable<AddressVM[]>;
    data = this.AddressService.getAllAddress();
    data.subscribe(
      (res: any) => {
        this.Address = res;
        this.loading.isLoading(false);
      },
      (error => {
        this.loading.isLoading(false);
        this.notification.ShowError(error.error.ExceptionMessage, error.message);
      })
    )
  }
  getCustomerdata() {
    this.loading.isLoading(true);
    let data: Observable<CustomerVM[]>;
    data = this.customerService.getAllCustomers();
    data.subscribe(
      (res: any) => {
        this.Customers = res;
        this.loading.isLoading(false);
      },
      (error => {
        this.loading.isLoading(false);
        this.notification.ShowError(error.error.ExceptionMessage, error.message);
      })
    )
  }
  display:string="none";
  openModel() {
    this.display="block";
    this.OpenAddressform = true;
    this.addressFormService.ismodel = true;
  }
  closeModel() {
    this.display="none";
    this.isEdit = false;
    this.addressFormService.ismodel = false;
    this.OpenAddressform = false;
    this.SelectedAddressId = null;
    this.getTabledata();
  }

  SelectedAddressId: number = null;
  OpenAddressform: boolean = false;

  edit(RecId: number) {
    debugger
    this.SelectedAddressId = RecId;
    this.OpenAddressform = true;
  }
  deleteId:number=null;
  delete(RecId: number) {
    //this.deleteId=RecId;
    debugger
    // if (!confirm("Are you sure want to delete?"))
    //   return;
    this.swalService.confirmBox();
    this.loading.isLoading(true);
    this.AddressService.deleteAddress(RecId).subscribe(
      (res => {
        this.loading.isLoading(false);
        this.notification.showSuccess("Address Deleted Successfully");
        this.getTabledata();
      }),
      (error => {
        this.loading.isLoading(false);
        this.notification.ShowError(error.error.ExceptionMessage, error.message);
      })
    );
   }
}
