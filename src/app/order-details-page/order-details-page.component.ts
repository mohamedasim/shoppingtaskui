import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { OrderService } from '../Services/Order.service';
import { ProductService } from '../Services/product.service';
import { CustomerService } from '../Services/customer.service';
import { AddressVM, CustomerVM, OrderItemCVM, OrderItemUVM, OrderItemVM, OrderVM, ProductVM } from '../Models/AllVM';
import { OrderItemService } from '../Services/OrderItem.service';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';

@Component({
  selector: 'app-order-details-page',
  templateUrl: './order-details-page.component.html',
  styleUrls: ['./order-details-page.component.css']
})
export class OrderDetailsPageComponent implements OnInit {
  OrderId:number;
  OrderItems:OrderItemVM[];
  Orders:OrderVM;
  ProductPrice:ProductVM;
  isEdit:boolean=false;
  Product:ProductVM[];
  Customers:CustomerVM[];
    constructor(private OrderService:OrderService,
      private CustomerService:CustomerService,
      private fb:FormBuilder,private ProductService:ProductService,
      private route:ActivatedRoute,public loading:LoadingService,
      private router:Router,private OrderItemService:OrderItemService,public notification:NotificationService) {
     }
    OrderItemForm:FormGroup=this.fb.group({
      RecId:[null],
      OrderId:[null,[Validators.required]],
      ProductId:[null,[Validators.required]],
      Quantity:[null,[Validators.required]],   
      Amount:[null,[Validators.required]]
    });
    ngOnInit(): void {
      this.route.paramMap.subscribe((res) => {
        this.OrderId = Number (res.get("OrdId"));
      });
      this.getOrderItemBasedOnOrderId();
      this.getOrderDetailsByRecId();
      this.getProductdata();
    }
    ProductPriceByRecId(){
     this.loading.isLoading(true);
      let data: Observable<ProductVM>;
      data =this.ProductService.getProductByRecId(this.OrderItemForm.value['ProductId']);
      data.subscribe(
        (res)=>{
          this.ProductPrice = res;
          this.loading.isLoading(false);
          this.OrderItemAmount();
        },
        (error =>{
          console.log(error);
          this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }
    price:number;
    quantity:number;
    sum:number;
    resetAmount(){
      this.price=null;
      this.quantity=null;
      this.sum=null;
    }
    OrderItemAmount(){debugger
      this.resetAmount();
    //   if(this.OrderItemForm.value['ProductId']){
    //   this.ProductPriceByRecId();
    // }
      this.price=this.ProductPrice.Price
      this.quantity=this.OrderItemForm.value['Quantity'];
      this.sum= this.price*this.quantity;
      this.OrderItemForm.patchValue({Amount:this.sum});
    }
    getOrderItemBasedOnOrderId(){
      this.loading.isLoading(true);
      let data: Observable<OrderItemVM[]>;
      data =this.OrderItemService.getOrderItemBasedOnOrderId(this.OrderId);
      data.subscribe(
        (res)=>{
          this.OrderItems = res;
          this.loading.isLoading(false);
        },
        (error =>{
          this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }
    getOrderDetailsByRecId(){
      this.loading.isLoading(true);
      let data: Observable<OrderVM>;
      data =this.OrderService.getOrderByRecId(this.OrderId);
      data.subscribe(
        (res)=>{
          this.Orders = res;
          this.loading.isLoading(false);
        },
        (error =>{
          this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }
    getProductdata(){
      this.loading.isLoading(true);
      let data: Observable<ProductVM[]>;
      data =this.ProductService.getAllProducts();
      data.subscribe(
        (res)=>{
          this.Product = res;
          this.loading.isLoading(false);
        },
        (error =>{
          console.log(error);
          this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      )
    }
    submit(submitData:OrderItemVM){debugger;
      this.loading.isLoading(true);
  if(submitData.RecId == null||submitData.RecId == undefined){
    const data:OrderItemCVM = {
      OrderId:submitData.OrderId,
      ProductId:submitData.ProductId,
      Quantity:submitData.Quantity,
      Amount:submitData.Amount
      };
     this.OrderItemService.createOrderItem(data).subscribe(
       (res =>{
         console.log(res);
         this.closeModel();
         this.OrderItemForm.reset();
         this.resetAmount();
         this.loading.isLoading(false);        
         this.notification.showSuccess("Created OrderItem Successfully.");
         this.getOrderItemBasedOnOrderId();
         this.getOrderDetailsByRecId();
       }),
       (error=>{
         this.closeModel();
         this.OrderItemForm.reset();
         this.resetAmount();
         this.loading.isLoading(false);
         this.notification.ShowError(error.error.ExceptionMessage,error.message);
       }),
     )
  }
  else{
    this.loading.isLoading(true);
    const data:OrderItemUVM={
      RecId:submitData.RecId,      
      OrderId:submitData.OrderId,
      ProductId:submitData.ProductId,
      Quantity:submitData.Quantity,
      Amount:submitData.Amount
    }
    this.OrderItemService.updateOrderItem(data).subscribe(
      (res =>{
        console.log(res);
        this.closeModel();
        this.OrderItemForm.reset();
        this.resetAmount();
        this.loading.isLoading(false);
        this.notification.showSuccess("Updated OrderItem Successfully.");
        this.getOrderItemBasedOnOrderId();
        this.getOrderDetailsByRecId();
      }),
      (error =>{
        this.closeModel();
        this.OrderItemForm.reset();
        this.resetAmount();
        this.loading.isLoading(false);
        this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    );
  }
    }
    display='none';
    openModel(){
      this.display='block'; 
      this.OrderItemForm.patchValue({OrderId:this.OrderId});
    }
closeModel(){
  this.display='none'; 
  this.OrderItemForm.reset();
  this.isEdit=false;
}
edit(RecId:number){debugger
  this.isEdit=true;
  this.loading.isLoading(true);
  this.resetAmount();
  this.OrderItemService.getOrderItemByRecId(RecId).subscribe(
    (res =>{
      console.log(res);
      this.OrderItemForm.patchValue(res);
      if(this.OrderItemForm.value['ProductId']){
        this.ProductPriceByRecId();
        }      
      this.loading.isLoading(false);
      this.openModel();
    }),
    (err =>{
      this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
    })
  )
}
delete(RecId:number){debugger
      if(!confirm("Are you sure want to delete?"))
      return;
      this.loading.isLoading(true);
      this.OrderItemService.deleteOrderItem(RecId).subscribe(
        (res =>{
          this.loading.isLoading(false);
          this.notification.showSuccess("Order item Deleted Successfully");
          this.getOrderItemBasedOnOrderId();
          this.getOrderDetailsByRecId();
        }),
        (error =>{
          this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
        })
      );
  
    }
}
