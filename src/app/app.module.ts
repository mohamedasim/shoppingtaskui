import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { RegisterPageComponent } from './register-page/register-page.component';
import { CustomerPageComponent } from './customer-page/customer-page.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './Services/user.service';
import { CustomerService } from './Services/customer.service';
import { ProductService } from './Services/product.service';
import { CustomerDetailsPageComponent } from './customer-details-page/customer-details-page.component';
import { ProductsPageComponent } from './products-page/products-page.component';
import { OrderPageComponent } from './order-page/order-page.component';
import { AddressPageComponent } from './address-page/address-page.component';
import { OrderService } from './Services/Order.service';
import { OrderItemService } from './Services/OrderItem.service';
import { AddressService } from './Services/Address.service';
import { AddressFormService } from './Services/address-form.service';
import { OrderDetailsPageComponent } from './order-details-page/order-details-page.component';
import { LoadingService } from './Services/Loading.service';
import { NotificationService } from './Services/notification.service';
import { NgxLoadingXConfig, POSITION, SPINNER, NgxLoadingXModule } from 'ngx-loading-x';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AddressFormComponent } from './address-form/address-form.component';
import { SwalService} from './Services/swal.service';

const ngxLoadingXConfig: NgxLoadingXConfig = {
  show: false,
  bgBlur: 2,
  bgOpacity: 5,
  bgLogoUrl: '',
  bgLogoUrlPosition: POSITION.centerCenter,
  bgLogoUrlSize: 100,
  spinnerType: SPINNER.wanderingCubes,
  spinnerSize: 120,
  spinnerColor: '#dd0031',
  spinnerPosition: POSITION.centerCenter,
}
@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HeaderComponent,
    FooterComponent,
    RegisterPageComponent,
    CustomerPageComponent,
    CustomerDetailsPageComponent,
    ProductsPageComponent,
    OrderPageComponent,
    AddressPageComponent,
    OrderDetailsPageComponent,
    AddressFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgxLoadingXModule.forRoot(ngxLoadingXConfig),
    BrowserAnimationsModule,
  	ToastrModule.forRoot()
  ],
  providers: [
    UserService,CustomerService,SwalService,
    ProductService,OrderService,AddressService,AddressFormService,
    OrderItemService,LoadingService,NotificationService
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
