export interface UserCVM{
    UserName:string;
    Password:string;
    }
    // export interface UserUVM extends UserCVM{
    // RecId:number;
    // }
    // export interface UserVM extends UserUVM{
        
    // }
export interface CustomerCVM{
    Name:string;
    Phone:number;
    Email:string;
}
export interface CustomerUVM extends  CustomerCVM{
    RecId:number;
}
export interface CustomerVM extends CustomerUVM{
    
}
export interface ProductCVM {
    Name:string;
    Description:string;
    Price:number;
}
export interface ProductUVM extends  ProductCVM{
    RecId:number;
}
export interface ProductVM extends ProductUVM{
    
}
export interface OrderCVM {
    Description:string;
    CustomerId:number,
    AddressId:number,
    PaymentType:ePaymentType,
    OrderDate:Date,
}
export interface OrderUVM extends  OrderCVM{
    RecId:number;
}
export interface OrderVM extends OrderUVM{
    TotalAmount:number;
}
export interface OrderItemCVM {
    OrderId:number;
    ProductId:number,
    Quantity:number,
    Amount:number
}
export interface OrderItemUVM extends  OrderItemCVM{
    RecId:number;
}
export interface OrderItemVM extends OrderItemUVM{
    Name:string;
}
export enum ePaymentType{
Cash=1,
Card=2
}
export interface AddressCVM {
    CustomerId:number,
    Street:string,
    Country:string,
    City:string,
}
export interface AddressUVM extends  AddressCVM{
    RecId:number;
}
export interface AddressVM extends AddressUVM{
    
}