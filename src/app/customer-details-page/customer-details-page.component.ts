import { Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AddressVM, CustomerVM,AddressCVM, AddressUVM, } from '../Models/AllVM';
import { CustomerService } from '../Services/customer.service';
import { AddressService } from '../Services/Address.service';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { AddressFormService } from '../Services/address-form.service';
import { NONE_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-customer-details-page',
  templateUrl: './customer-details-page.component.html',
  styleUrls: ['./customer-details-page.component.css']
})
export class CustomerDetailsPageComponent implements OnInit {
  Customer:CustomerVM;
  CustomerId:number;
  isEdit:boolean=false;
  Address:AddressVM[];
  editAddress:AddressUVM[];

  constructor(public route:ActivatedRoute,private customerService:CustomerService,
    private fb:FormBuilder,
    public AddressService:AddressService,
    public loading:LoadingService,public notification:NotificationService,
    public addressFormService:AddressFormService) { }
     

  ngOnInit(): void {
     this.route.paramMap.subscribe((res) => {
      this.CustomerId = Number (res.get("RecId"));
    });
      this.getCustomerDetail();
      this.getAddressBasedOnCustomer();
  }
  getCustomerDetail(){
    debugger;
    this.loading.isLoading(true);
    let data: Observable<CustomerVM>;
    data =this.customerService.getCustomerByRecId(this.CustomerId)
    data.subscribe(
    (res =>{
      this.Customer=res;
      this.loading.isLoading(false);
    }),
    (err =>{
      this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
    })
  );
  }

  getAddressBasedOnCustomer(){ 
    debugger;
    this.loading.isLoading(true);
    let data: Observable<AddressVM[]>;
    data =this.AddressService.getAddressBasedOnCustomerRecId(this.CustomerId)
    data.subscribe(
    (res =>{
      this.Address=res;
      this.loading.isLoading(false);
    }),
    (err =>{
      this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
    })
  );
  }
  display:string="none";
  openModel(){
    this.display="block";
    this.addressFormService.ismodel=true;
    this.openAddressForm = true;
  }
  closeModel(){
    this.display="none";
    this.addressFormService.ismodel = false;
    this.openAddressForm = false;
    this.AddressId = null;
    this.getCustomerDetail();
    this.getAddressBasedOnCustomer();
  }
  submit(submitData:AddressVM){debugger;
    this.loading.isLoading(true);
if(submitData.RecId == null||submitData.RecId == undefined){
  const data:AddressCVM = {
    CustomerId:submitData.CustomerId,
    Street:submitData.Street,
    Country:submitData.Country,
    City:submitData.City
  };
   this.AddressService.createAddress(data).subscribe(
     (res =>{
       this.loading.isLoading(false);
       this.notification.showSuccess("Address Created Successfully");
       this.getAddressBasedOnCustomer();
     }),
     (error=>{
       this.loading.isLoading(false);
       this.notification.ShowError(error.error.ExceptionMessage,error.message);
     }),
   )
}
else{
  this.loading.isLoading(true);
  const data:AddressUVM={
    RecId:submitData.RecId,
    CustomerId:submitData.CustomerId,
    Street:submitData.Street,
    Country:submitData.Country,
    City:submitData.City
  }
  this.AddressService.updateAddress(data).subscribe(
    (res =>{
      this.loading.isLoading(false);
      this.notification.showSuccess("Address Updated Successfully");
      this.getAddressBasedOnCustomer();
    }),
    (error =>{
      this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
    })
  );
}
  }
  openAddressForm:boolean=false;
  AddressId:number;
  edit(RecId:number){debugger
    this.isEdit=true;
    this.openAddressForm=true;
    this.AddressId=RecId;
    //this.openModel();
  }
  delete(RecId:number){debugger
    if(!confirm("Are you sure want to delete?"))
    return;
    this.loading.isLoading(true);
    this.AddressService.deleteAddress(RecId).subscribe(
      (res =>{
        this.loading.isLoading(false);
        this.notification.showSuccess("Address Deleted Successfully.");
        this.getAddressBasedOnCustomer();
      }),
      (error =>{
        this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    );

  }

}
