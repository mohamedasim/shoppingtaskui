import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductCVM,ProductUVM,ProductVM} from '../Models/AllVM';
import { Product} from './all-api.service';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  constructor(private http:HttpClient,) { }
  getAllProducts(){debugger
    const url = Product.getAllProducts();
    return this.http.get<ProductVM[]>(url);
  }
  getProductByRecId(RecId:number){
    const url = Product.getProductByRecId(RecId);
    return this.http.get<ProductVM>(url);
  }
  createProduct(data:ProductCVM){debugger
    const url = Product.postProduct();
    return this.http.post(url,data);
  }
  updateProduct(data:ProductUVM){debugger
    const url = Product.putProduct();
    return this.http.put(url,data);
  }
  deleteProduct(RecId:number){debugger
    const url = Product.deleteProduct(RecId);
    return this.http.delete(url);
  }
}
