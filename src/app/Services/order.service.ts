import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderCVM,OrderUVM,OrderVM} from '../Models/AllVM';
import { Order} from './all-api.service';
@Injectable({
  providedIn: 'root'
})
export class OrderService {
  constructor(private http:HttpClient,) { }
  getAllOrders(){debugger
    const url = Order.getAllOrders();
    return this.http.get<OrderVM[]>(url);
  }
  getOrderByRecId(RecId:number){
    const url = Order.getOrderByRecId(RecId);
    return this.http.get<OrderVM>(url);
  }
  createOrder(data:OrderCVM){debugger
    const url = Order.postOrder();
    return this.http.post(url,data);
  }
  updateOrder(data:OrderUVM){debugger
    const url = Order.putOrder();
    return this.http.put(url,data);
  }
  deleteOrder(RecId:number){debugger
    const url = Order.deleteOrder(RecId);
    return this.http.delete(url);
  }
}
