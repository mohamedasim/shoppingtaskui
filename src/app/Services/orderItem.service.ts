import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OrderItemCVM,OrderItemUVM,OrderItemVM} from '../Models/AllVM';
import { OrderItem} from './all-api.service';
@Injectable({
  providedIn: 'root'
})
export class OrderItemService {
  constructor(private http:HttpClient,) { }
  getAllOrderItem(){debugger
    const url = OrderItem.getAllOrderItem();
    return this.http.get<OrderItemVM[]>(url);
  }
  getOrderItemByRecId(RecId:number){
    const url = OrderItem.getOrderItemByRecId(RecId);
    return this.http.get(url);
  }
  getOrderItemBasedOnOrderId(RecId:number){
    const url = OrderItem.getOrderItemBasedOnOrderId(RecId);
    return this.http.get<OrderItemVM[]>(url);
  }
  createOrderItem(data:OrderItemCVM){debugger
    const url = OrderItem.postOrderItem();
    return this.http.post(url,data);
  }
  updateOrderItem(data:OrderItemUVM){debugger
    const url = OrderItem.putOrderItem();
    return this.http.put(url,data);
  }
  deleteOrderItem(RecId:number){debugger
    const url = OrderItem.deleteOrderItem(RecId);
    return this.http.delete(url);
  }
}
