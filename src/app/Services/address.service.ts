import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AddressCVM,AddressUVM,AddressVM} from '../Models/AllVM';
import { Address} from './all-api.service';
@Injectable({
  providedIn: 'root'
})
export class AddressService {
  constructor(private http:HttpClient,) { }
  getAllAddress(){debugger
    const url = Address.getAllAddress();
    return this.http.get<AddressVM[]>(url);
  }
  getAddressByRecId(RecId:number){
    const url = Address.getAddressByRecId(RecId);
    return this.http.get<AddressVM[]>(url);
  }
  getAddressBasedOnCustomerRecId(RecId:number){
    const url = Address.getAddressBasedOnCustomerRecId(RecId);
    return this.http.get<AddressVM[]>(url);
  }
  createAddress(data:AddressCVM){debugger
    const url = Address.postAddress();
    return this.http.post(url,data);
  }
  updateAddress(data:AddressUVM){debugger
    const url = Address.putAddress();
    return this.http.put(url,data);
  }
  deleteAddress(RecId:number){debugger
    const url = Address.deleteAddress(RecId);
    return this.http.delete(url);
  }
}
