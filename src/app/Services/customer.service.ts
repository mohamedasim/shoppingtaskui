import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomerCVM,CustomerUVM,CustomerVM} from '../Models/AllVM';
import { Customer} from '../Services/all-api.service';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  constructor(private http:HttpClient,) { }
  getAllCustomers(){debugger
    const url = Customer.getAllCustomers();
    return this.http.get<CustomerVM[]>(url);
  }
  getCustomerByRecId(RecId:number){
    const url = Customer.getCustomerByRecId(RecId);
    return this.http.get<CustomerVM>(url);
  }
  createCustomer(data:CustomerCVM){debugger
    const url = Customer.postCustomer();
    return this.http.post(url,data);
  }
  updateCustomer(data:CustomerUVM){debugger
    const url = Customer.putCustomer();
    return this.http.put(url,data);
  }
  deleteCustomer(RecId:number){debugger
    const url = Customer.deleteCustomer(RecId);
    return this.http.delete(url);
  }
}
