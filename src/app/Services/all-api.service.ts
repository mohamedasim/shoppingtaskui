import { environment } from '../../environments/environment';

export const User={
postUser:()=>
`${environment.clientUrl}/api/UserRegisterCreate`,
getAllUser:()=>
`${environment.clientUrl}/api/UserRegisterList`,
validateUser:()=>
`${environment.clientUrl}/api/UserRegisterValidation`,
}
export const Customer={
  getAllCustomers:()=>
  `${environment.clientUrl}/api/getAllCustomers`,
  getCustomerByRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getCustomerByRecId/${RecId}`,
  postCustomer:()=>
  `${environment.clientUrl}/api/CreateCustomer`,
  putCustomer:()=>
  `${environment.clientUrl}/api/UpdateCustomer`,
  deleteCustomer:(RecId:number)=>
  `${environment.clientUrl}/api/DeleteCustomer/${RecId}`,
}
export const Product={
  getAllProducts:()=>
  `${environment.clientUrl}/api/getAllProducts`,
  getProductByRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getProductsByRecId/${RecId}`,
  getOrdersBasedOnProductId:(RecId:number)=>
  `${environment.clientUrl}/api/getOrdersBasedOnProductId/${RecId}`,
  postProduct:()=>
  `${environment.clientUrl}/api/CreateProducts`,
  putProduct:()=>
  `${environment.clientUrl}/api/UpdateProducts`,
  deleteProduct:(RecId:number)=>
  `${environment.clientUrl}/api/DeleteProducts/${RecId}`,
}
export const Order={
  getAllOrders:()=>
  `${environment.clientUrl}/api/getAllOrders`,
  getOrderByRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getOrdersByRecId/${RecId}`,
  postOrder:()=>
  `${environment.clientUrl}/api/CreateOrders`,
  putOrder:()=>
  `${environment.clientUrl}/api/UpdateOrders`,
  deleteOrder:(RecId:number)=>
  `${environment.clientUrl}/api/DeleteOrders/${RecId}`,
}
export const OrderItem={
  getAllOrderItem:()=>
  `${environment.clientUrl}/api/getAllOrdersItem`,
  getOrderItemByRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getOrdersItemByRecId/${RecId}`,
  getOrderItemBasedOnOrderId:(RecId:number)=>
  `${environment.clientUrl}/api/GetOrderItemBasedOnOrderId/${RecId}`,
  postOrderItem:()=>
  `${environment.clientUrl}/api/CreateOrdersItem`,
  putOrderItem:()=>
  `${environment.clientUrl}/api/UpdateOrdersItem`,
  deleteOrderItem:(RecId:number)=>
  `${environment.clientUrl}/api/DeleteOrdersItem/${RecId}`,
}
export const Address={
  getAllAddress:()=>
  `${environment.clientUrl}/api/getAllAddress`,
  getAddressByRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getAddressByRecId/${RecId}`,
  getAddressBasedOnCustomerRecId:(RecId:number)=>
  `${environment.clientUrl}/api/getAddressBasedOnCustomerRecId/${RecId}`,
  postAddress:()=>
  `${environment.clientUrl}/api/CreateAddress`,
  putAddress:()=>
  `${environment.clientUrl}/api/UpdateAddress`,
  deleteAddress:(RecId:number)=>
  `${environment.clientUrl}/api/DeleteAddress/${RecId}`,
}