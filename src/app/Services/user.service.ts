import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User} from '../Services/all-api.service';
import { UserCVM } from '../Models/AllVM';
import { CanActivate } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate{
  constructor(private http:HttpClient,) {
  //   if(!this.getlocalLoginStatus())
  //   return
  }
  canActivate(){
    if(this.getlocalLoginStatus())
    return true;
  }
  postUser(data:UserCVM){debugger
    const url = User.postUser();
    return this.http.post(url,data);
  }
  validateUser(data:UserCVM){debugger
    const url = User.validateUser();
    return this.http.post(url,data);
  }
  Storeloginname(loginname) {
    debugger
    let tabledata = loginname;
    let data = JSON.stringify(tabledata)
    localStorage.setItem('LoginName', data)
    return tabledata;
  }
  getlocalloginname() {
    let log = JSON.parse(localStorage.getItem('LoginName'));
    return log;
  }
  StoreloginStatus(logstatus) {
    debugger
    let tabledata = logstatus;
    let data = JSON.stringify(tabledata)
    localStorage.setItem('LoginStatus', data)
    if(!this.getlocalLoginStatus())
    localStorage.removeItem('LoginName');
    return tabledata;
  }
  getlocalLoginStatus() {
    let log = JSON.parse(localStorage.getItem('LoginStatus'));
    return log;
  }
}
