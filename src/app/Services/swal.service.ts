import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.js'; 
@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor() { }

  confirmBox(){  
    Swal.fire({  
      title: 'Are you sure want to  delete?',  
      text: 'You will not be able to recover this file!',  
      icon: 'warning',  
      showCancelButton: true,  
      confirmButtonText: 'Yes, delete it!',  
      cancelButtonText: 'No, keep it'  
    }).then((result) => {  
      if (result.value) {  
        Swal.fire(  
          'Deleted!',  
          'Your imaginary file has been deleted.',  
          'success'  
        )  
      } else if (result.dismiss === Swal.DismissReason.cancel) {  
        Swal.fire(  
          'Cancelled',  
          'Your imaginary file is safe :)',  
          'error'  
        )  
      }  
    })  
  }  
}
