import { Component } from '@angular/core';
import {FormGroup, FormControl, Validators}  from '@angular/forms';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { LoadingService } from './Services/Loading.service';
import { UserService } from './Services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ShoppingTaskUI';
  public loading = false;
  constructor(public service:LoadingService,public userService:UserService,public router:Router,public route:ActivatedRoute){
    this.loading=false;

   router.events.subscribe(events=>{
     if(events instanceof NavigationStart){
      if(events.url=="/Login"){
        this.userService.StoreloginStatus(false);
      }
    }
   })

  }
}
