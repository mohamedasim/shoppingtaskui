import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomerCVM, CustomerUVM, CustomerVM } from '../Models/AllVM';
import { CustomerService } from '../Services/customer.service';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { UserService } from '../Services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
@Component({
  selector: 'app-customer-page',
  templateUrl: './customer-page.component.html',
  styleUrls: ['./customer-page.component.css']
})
export class CustomerPageComponent implements OnInit {
Customers:CustomerVM[];
isEdit:boolean=false;
  constructor(private CustomerService:CustomerService,private fb:FormBuilder,
    private userService:UserService,private route:ActivatedRoute,
    private router:Router,public loading:LoadingService,public notification:NotificationService) { }

  CustomerForm:FormGroup=this.fb.group({
    RecId:[null],
    Name:[null,[Validators.required,Validators.minLength(4)]],
    Phone:[null,[Validators.required,Validators.pattern("^[0-9]{10}$")]],
    Email:[null,[Validators.required,Validators.email]]
  })
  ngOnInit(): void {
    this.getTabledata();
  }

  getTabledata(){
    this.loading.isLoading(true);
    let data: Observable<CustomerVM[]>;
    data =this.CustomerService.getAllCustomers();
    data.subscribe(
      (res:any)=>{
        this.Customers = res;
        this.loading.isLoading(false);
      },
      (error =>{
        this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    )
  }
  display='none';
  openModel(){
    this.display='block'; 
  }
  closeModel(){
    this.display='none';
    this.CustomerForm.reset();
    this.isEdit=false;
  }
  submit(submitData:CustomerVM){debugger;
    this.loading.isLoading(true);
if(submitData.RecId == null||submitData.RecId == undefined){
  const data:CustomerCVM = {
Name:submitData.Name,
Phone:submitData.Phone,
Email:submitData.Email
  };
   this.CustomerService.createCustomer(data).subscribe(
     (res =>{
       console.log(res);
       this.closeModel();
       this.CustomerForm.reset();
       this.loading.isLoading(false);
       this.notification.showSuccess("Customer Created SuccessFully.");
       this.getTabledata();
     }),
     (error=>{
       this.closeModel();
       this.CustomerForm.reset();
       this.loading.isLoading(false);
       this.notification.ShowError(error.error.ExceptionMessage,error.message);
     }),
   )
}
else{
  this.loading.isLoading(true);
  const data:CustomerUVM={
    RecId:submitData.RecId,
    Name:submitData.Name,
    Phone:submitData.Phone,
    Email:submitData.Email
  }
  this.CustomerService.updateCustomer(data).subscribe(
    (res =>{
      this.closeModel();
      this.CustomerForm.reset();
      this.loading.isLoading(false);
      this.notification.showSuccess("Customer Updated Successfully.");
      this.getTabledata();
    }),
    (error =>{
      this.closeModel();
      this.CustomerForm.reset();
      this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
    })
  );
}
  }
  edit(RecId:number){debugger
    this.isEdit=true;
    this.loading.isLoading(true);
    this.CustomerService.getCustomerByRecId(RecId).subscribe(
      (res =>{
        console.log(res);
        this.CustomerForm.patchValue(res);
        this.loading.isLoading(false);
        this.openModel();
      }),
      (err =>{
        this.loading.isLoading(false);
      this.notification.ShowError(err.error.ExceptionMessage,err.message);
      })
    )
  }
  delete(RecId:number){debugger
    if(!confirm("Are you sure want to delete?"))
    return;
    this.loading.isLoading(true);
    this.CustomerService.deleteCustomer(RecId).subscribe(
      (res =>{
        this.loading.isLoading(false);
        this.notification.showSuccess("Customer Deleted Successfully");
        this.getTabledata();
      }),
      (error =>{
        this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    );
  }
  goDetails(RecId:number,CusId:number){
    this.router.navigate(['/Customer-details',RecId]);
  }
}
