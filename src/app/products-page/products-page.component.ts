import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup,FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { ProductVM,ProductCVM,ProductUVM} from '../Models/AllVM';
import { LoadingService } from '../Services/Loading.service';
import { NotificationService } from '../Services/notification.service';
import { ProductService } from '../Services/product.service';

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.css']
})
export class ProductsPageComponent implements OnInit {
  Products:ProductVM[];
  isEdit:boolean=false;
isPopUpOpen:boolean;
  ProductForm:FormGroup=this.fb.group({
    RecId:[null],
    Name:[null,[Validators.required,Validators.minLength(4)]],
    Description:[null,[Validators.required]],
    Price:[null,[Validators.required]]
  })
  constructor(private fb:FormBuilder,private productService:ProductService,
    public loading:LoadingService,public notification:NotificationService) { }

  ngOnInit(): void {
    this.getTabledata();
  }
  getTabledata(){
    this.loading.isLoading(true);
    let data: Observable<ProductVM[]>;
    data =this.productService.getAllProducts();
    data.subscribe(
      (res:any)=>{
        this.Products = res;
        this.loading.isLoading(false);
      },
      (error =>{
        this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    )
  }
  display='none';
  openModel(){
    this.display='block';
  }
  closeModel(){
    this.display='none';
    this.ProductForm.reset();
    this.isEdit=false;
  }
  submit(submitData:ProductVM){debugger;
    this.loading.isLoading(true);
if(submitData.RecId == null||submitData.RecId == undefined){
  const data:ProductCVM = {
Name:submitData.Name,
Description:submitData.Description,
Price:submitData.Price
  };
   this.productService.createProduct(data).subscribe(
     (res =>{
       this.closeModel();
       this.ProductForm.reset();
       this.loading.isLoading(false);
       this.notification.showSuccess("Product Created Successfully.");
       this.getTabledata();
     }),
     (error=>{
       this.closeModel();
       this.ProductForm.reset();
       this.loading.isLoading(false);
       this.notification.ShowError(error.error.ExceptionMessage,error.message);
     }),
   )
}
else{
  this.loading.isLoading(true);
  const data:ProductUVM={
    RecId:submitData.RecId,
    Name:submitData.Name,
    Description:submitData.Description,
    Price:submitData.Price
  }
  this.productService.updateProduct(data).subscribe(
    (res =>{
      this.closeModel;
      this.ProductForm.reset();
      this.loading.isLoading(false);
      this.notification.showSuccess("Product Updated Successfully");
      this.getTabledata();
    }),
    (error =>{
      this.closeModel();
      this.ProductForm.reset();
      this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
    })
  );
}
  }
  edit(RecId:number){debugger
    this.isEdit=true;
    this.loading.isLoading(true);
    this.productService.getProductByRecId(RecId).subscribe(
      (res =>{
        this.ProductForm.patchValue(res);
        this.openModel();
        this.loading.isLoading(false);
      }),
      (err =>{
        this.loading.isLoading(false);
        this.notification.ShowError(err.error.ExceptionMessage,err.message);
      })
    )
  }
  delete(RecId:number){debugger
    if(!confirm("Are you sure want to delete?"))
    return;
    this.loading.isLoading(true);
    this.productService.deleteProduct(RecId).subscribe(
      (res =>{
        this.loading.isLoading(false);
        this.notification.showSuccess("Product Deleted Successfully");
        this.getTabledata();
      }),
      (error =>{
        this.loading.isLoading(false);
      this.notification.ShowError(error.error.ExceptionMessage,error.message);
      })
    );
  }
}
